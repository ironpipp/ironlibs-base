let isNode : boolean = (new Function("try {return this===global;}catch(e){return false;}"))();      //best way to detect until now (ott 2020)

export module IronLibsCommon
{
    /*##########################################################################
     #########################   VERY GENERIC METHODS   #########################
     ##########################################################################*/


    let __ = IronLibsCommon;
    let currentLanguage : string = null;
    let isDebug = false;

    let uuidModuleInstance : any;
    let sprintfModuleInstance : any;


    /**
     * Changes the actual DEBUG MODE
     *
     * @method SetDebugMode
     * @param val The value to set
     */
    export function SetDebugMode(val : boolean) : void
    {
        isDebug = val;
    }


    /**
     * Returns TRUE if application is configured to be in DEBUG MODE
     *
     * @method IsDebugMode
     * @return {boolean}
     */
    export function IsDebugMode() : boolean
    {
        return isDebug;
    }


    /**
     * log to console one or MORE passed arguments
     */
    export function Log(...params : any[]) : void
    {
        //F.P. unfortunately we can't use console.log.apply, so switch on arguments length

        let when = __.Dates.FormatDate(new Date());

        if (arguments.length == 1)
            console.log(when, arguments[0]);
        else if (arguments.length == 2)
            console.log(when, arguments[0], arguments[1]);
        else if (arguments.length == 3)
            console.log(when, arguments[0], arguments[1], arguments[2]);
        else if (arguments.length == 4)
            console.log(when, arguments[0], arguments[1], arguments[2], arguments[3]);
        else if (arguments.length == 5)
            console.log(when, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
        else if (arguments.length == 6)
            console.log(when, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
        else
            console.log(when, arguments);
    }

    export function LogFatal(...params : any[]) : void
    {
        Log.apply(this, ["## FATAL ## "].concat(params));
    }

    export function LogError(...params : any[]) : void
    {
        Log.apply(this, ["## ERROR ## "].concat(params));
    }

    export function LogWarn(...params : any[]) : void
    {
        Log.apply(this, ["## WARN  ## "].concat(params));
    }

    export function LogDebug(...params : any[]) : void
    {
        Log.apply(this, ["-- DEBUG -- "].concat(params));
    }


    /**
     * Formats a string with N parameters of any type.
     * Wrapper of sprintf
     *
     * @param formatString
     * @param params
     * @returns {string} the formatted string
     */
    export function Format(formatString : string, ...params : any[]) : string
    {
        if (IsNull(sprintfModuleInstance))
        {
            if (isNode)
                sprintfModuleInstance = require("sprintf-js").sprintf;
            else
                sprintfModuleInstance = window["sprintf"];
        }

        return sprintfModuleInstance.apply(this, arguments);
    }


    /**
     * Returns the most suitable size format for the given number
     *
     * @method FormatFileSize
     * @param {number|string} fileSize  The number of BYTES for format
     * @param {boolean} shortForm  If TRUE returns the 2-characters representation
     * @return {string}
     */
    export function FormatFileSize(fileSize : number | string, shortForm? : boolean) : string
    {
        if (IsString(fileSize))
            fileSize = parseFloat(<string>fileSize);

        if (isNaN(<number>fileSize))
            return "";

        let formatString = "%.2f %s";
        if (fileSize < 1024)
            return fileSize + (shortForm == true ? " B" : " Bytes");
        else if (fileSize < 1024 * 1024)
            return Format(formatString, <number>fileSize / 1024, shortForm == true ? "KB" : "KBytes");
        else if (fileSize < 1024 * 1024 * 1024)
            return Format(formatString, <number>fileSize / (1024 * 1024), shortForm == true ? "MB" : "MBytes");
        else
            return Format(formatString, <number>fileSize / (1024 * 1024 * 1024), shortForm == true ? "GB" : "GBytes");
    }



    /**
     * Returns TRUE if obj is NULL or UNDEFINED
     *
     * @method IsNull
     * @param obj What to check
     * @return {boolean}
     */
    export function IsNull(obj) : obj is null
    {
        return (obj === undefined || obj === null);
    }


    /**
     * Returns TRUE if obj isn't NULL or UNDEFINED
     *
     * @method IsNotNull
     * @param obj What to check
     * @return {boolean}
     */
    export function IsNotNull(obj) : boolean
    {
        return (obj !== undefined && obj !== null);
    }



    /**
     * Returns TRUE if obj IS an object and isn't NULL or UNDEFINED
     *
     * @method IsNotNullObject
     * @param obj What to check
     * @return {boolean}
     */
    export function IsNotNullObject(obj) : boolean
    {
        return obj !== undefined && obj !== null && typeof (obj) === "object";
    }


    /**
     * Returns TRUE if obj is a FUNCTION
     *
     * @method IsFunction
     * @param obj What to check
     * @return {boolean}
     */
    export function IsFunction(obj : any) : obj is Function
    {
        return typeof (obj) === "function";
    }


    /**
     * Returns TRUE if obj is a NUMBER
     *
     * @method IsNumber
     * @param obj What to check
     * @return {boolean}
     */
    export function IsNumber(obj) : obj is number
    {
        return typeof obj === "number" && isNaN(obj) == false;
    }


    /**
     * Returns TRUE if obj is a Date instance
     *
     * @method IsDate
     * @param obj What to check
     * @return {boolean}
     */
    export function IsDate(obj) : obj is Date
    {
        return (IsNotNull(obj) && (obj instanceof Date) && __.IsNumber(obj.getDate()));
    }


    export enum RoundingMethod
    {
        FLOOR,
        ROUND,
        NO_ROUNDING
    }

    /**
     * Returns a number with precision clamped to a fixed resolution
     * Ex: LimitNumberPrecision(1.12345678, 3) == 1.123
     *
     * ATTENTION: JavaScript is tricky/buggy! Here some examples:
     *
     * 2.05*100 == 204.99999999999997  not 205  so
     *
     * LimitNumberPrecision(2.05, 2, __.RoundingMethod.FLOOR) == 2.04
     *
     * LimitNumberPrecision(2.05, 2, __.RoundingMethod.NO_ROUNDING) == 2.05
     *
     * @param num The float number to limit
     * @param precision the number of DIGITS after "."
     * @param method The method used to "cut" digits: Math.round, Math.floor or basic extra digits removal with no rounding
     *
     */
    export function LimitNumberPrecision(num : number, precision : number, method : RoundingMethod = RoundingMethod.ROUND) : number
    {
        if (!__.IsNumber(num))
            return num;

        let multiplier = Math.pow(10, precision);

        if (method == RoundingMethod.ROUND)
            return Math.round((num * multiplier)) / multiplier;
        else if (method == RoundingMethod.FLOOR)
            return Math.floor((num * multiplier)) / multiplier;
        else
        {
            let str = num.toString();
            let dotPos = str.indexOf(".");
            if (dotPos < 0)
                return num;
            str = str.substr(0, dotPos + precision + 1);
            return parseFloat(str);
        }

        //https://www.jacklmoore.com/notes/rounding-in-javascript/
        //return Number(Math.floor(<any>(num + 'e' + precision)) + 'e-' + precision);
    }



    /**
     * Returns TRUE if obj is a native BOOLEAN instance
     *
     * @method IsBoolean
     * @param obj What to check
     * @return {boolean}
     */
    export function IsBoolean(obj) : obj is boolean
    {
        return typeof obj === "boolean";
    }


    /**
     * Returns TRUE if obj is a NOT-NULL NUMBER, BOOLEAN, DATE or STRING
     *
     * @method IsNativeType
     * @param obj What to check
     * @return {boolean}
     */
    export function IsNativeType(obj) : obj is (number | boolean | string | Date)
    {
        return IsNumber(obj) || IsBoolean(obj) || IsString(obj) || IsDate(obj);
    }



    /**
     * Returns TRUE if obj is an ARRAY
     *
     * @method IsArray
     * @param obj What to check
     * @return {boolean}
     */
    export function IsArray(obj) : obj is any[]
    {
        return IsNotNull(obj) && obj instanceof Array;
    }


    /**
     * Returns TRUE if obj is a STRING
     *
     * @method IsString
     * @param obj What to check
     * @return {boolean}
     */
    export function IsString(obj) : obj is string
    {
        return typeof obj == "string";
    }


    /**
     * Returns TRUE if obj is NULL or an EMPTY STRING
     *
     * @method IsEmptyString
     * @param obj What to check
     * @return {boolean}
     */
    export function IsEmptyString(obj) : obj is ""
    {
        return IsNull(obj) || obj === "";
    }



    /**
     * Returns TRUE if obj is NULL or an EMPTY OBJECT (without any property)
     *
     * @method IsEmptyObject
     * @param obj What to check
     * @return {boolean}
     */
    export function IsEmptyObject(obj) : obj is Object
    {
        if (IsNull(obj))
            return true;

        if (IsNativeType(obj))
            return false;

        for (let k in obj)
        {
            if (!obj.hasOwnProperty(k))
                continue;
            return false;
        }

        return true;
    }


    /**
     * Returns TRUE if obj is a STRING and is not empty
     * (WARNING: can have only empty spaces! To check them use IsNotWhiteSpace)
     *
     * @param {string|*} obj
     * @return {boolean}
     */
    export function IsNotEmptyString(obj) : boolean
    {
        return IsString(obj) && obj.length > 0;
    }


    /**
     * Returns TRUE if obj is NULL, an EMPTY STRING or an EMPTY ARRAY
     *
     * @method IsNullOrEmpty
     * @param obj What to check
     * @return {boolean}
     */
    export function IsNullOrEmpty(obj : string | any) : obj is (null | "")
    {
        return (IsEmptyString(obj)) || (IsArray(obj) && obj.length == 0);
    }


    /**
     * Returns TRUE if obj is an ARRAY AND has at least one element
     * @return {boolean}
     */
    export function IsNotEmptyArray(obj) : boolean
    {
        return IsArray(obj) && obj.length > 0;
    }



    /**
     * Returns TRUE if the passed object is a jquery-wrapped object.
     * (automatic check of NOT-NULL)
     *
     * @method IsJquery
     * @param obj What to check
     * @return {boolean}
     */
    export function IsJquery(obj) : boolean     //can't return "obj is JQuery" otherwise we force a package dependency for library users
    {
        return isNode == false && window != null && obj instanceof window["$"];
    }



    /**
     * Returns TRUE if the passed object is a string representing a valid EMAIL address.
     * (automatic check of NOT-NULL)
     *
     * @method IsEmail
     * @param obj What to check
     * @return {boolean}
     */
    export function IsEmail(obj) : obj is string
    {
        //F.P. This is the default jQuery validate implementation BUT is TOO RELAXED (es: accepts "asd@asd")  (from https://html.spec.whatwg.org/multipage/input.html#valid-e-mail-address )
        //return __.IsString(obj) && /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test( obj );

        //prefer this (from https://emailregex.com/ )
        return __.IsString(obj) && /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(obj);
    }


    const PLUS_ONLY = /\+.*$/;
    const PLUS_AND_DOT = /\.|\+.*$/g;
    const normalizableProviders = {
        'gmail.com'      : {
            'cut' : PLUS_AND_DOT
        },
        'googlemail.com' : {
            'cut'     : PLUS_AND_DOT,
            'aliasOf' : 'gmail.com'
        },
        'hotmail.com'    : {
            'cut' : PLUS_ONLY
        },
        'live.com'       : {
            'cut' : PLUS_AND_DOT
        },
        'outlook.com'    : {
            'cut' : PLUS_ONLY
        }
    };

    /**
     * Returns the passed email address:
     * - lower case domain (RFC 1035 "Name servers and resolvers must compare [domains] in a case-insensitive manner")
     * and for some domains (which consider the emails equivalent):
     * - without dots
     * - without substring after "+"
     *
     * @method NormalizeEmail
     */
    export function NormalizeEmail(eMail : string) : string
    {
        if (!IsString(eMail))
        {
            return eMail;       //original passed object
        }

        let email = eMail.toString();                   //clone it
        let emailParts = email.split(/@/);

        if (emailParts.length !== 2)
        {
            return eMail;       //original passed object
        }

        let username = emailParts[0];
        let domain = emailParts[1].toLowerCase();

        if (normalizableProviders.hasOwnProperty(domain))
        {
            if (normalizableProviders[domain].hasOwnProperty('cut'))
            {
                username = username.replace(normalizableProviders[domain].cut, '');
            }
            if (normalizableProviders[domain].hasOwnProperty('aliasOf'))
            {
                domain = normalizableProviders[domain].aliasOf;
            }
        }

        return username + '@' + domain;     //USERNAME eventually sanitized, DOMAIN forced LOWER CASE
    }


    /**
     * Returns ALWAYS a string: the representation of the passed object:
     * EMPTY string upon NULL, or toString() upon OBJECT
     *
     * @param {*} str
     * @param {string} defaultValue
     * @return {string}
     */
    export function EnsureString(str : any, defaultValue : string = "") : string
    {
        if (IsNull(str))
            return defaultValue;
        else if (IsString(str))
            return str;

        return str.toString();
    }


    /**
     * Returns ALWAYS something NOT NULL (which can be specified as defaultValue).
     * Similar to {{#crossLink "__/EnsureString"}}{{/crossLink}} but for objects.
     *
     * @method EnsureNotNull
     * @param obj Anything to be checked
     * @param {string} defaultValue What to return in case str is NULL. Defaults to {}
     * @return {object}
     */
    export function EnsureNotNull(obj : any, defaultValue : any = {}) : any
    {
        if (__.IsNull(obj))
            return defaultValue;
        return obj;
    }


    /**
     * If "str" ends with "endsWith" returns TRUE, FALSE otherwise.
     * Parameters are always string-ensured.
     *
     * @method StringEndsWith
     * @param str The string to check
     * @param endsWith The ending to check
     * @param caseSensitive CASE check, defaults to TRUE
     */
    export function StringEndsWith(str : any, endsWith : string, caseSensitive : boolean = true) : boolean
    {
        str = EnsureString(str);
        endsWith = EnsureString(endsWith);
        if (!caseSensitive)
        {
            str = str.toLowerCase();
            endsWith = endsWith.toLowerCase();
        }
        let ret = str;
        let pos = ret.lastIndexOf(endsWith);
        return !(pos < 0 || pos != ret.length - endsWith.length);
    }


    /**
     * Returns always a NEW string ending with "endsWith" parameter.
     * If the passed "str" wasn't already ending with "endsWith", this will be appended.
     * Parameters are always string-ensured.
     *
     * @method EnsureEndsWith
     * @param str
     * @param endsWith
     * @param caseSensitive
     */
    export function EnsureEndsWith(str : any, endsWith : string, caseSensitive : boolean = true) : string
    {
        str = EnsureString(str);
        endsWith = EnsureString(endsWith);

        if (StringEndsWith(str, endsWith, caseSensitive))
            return str;
        return str + endsWith;
    }


    /**
     * If "str" starts with "startsWith" returns TRUE, FALSE otherwise.
     * Parameters are always string-ensured.
     *
     * @method StringStartsWith
     * @param str The string to check
     * @param startsWith The starting string to check
     * @param caseSensitive CASE check, defaults to TRUE
     */
    export function StringStartsWith(str : any, startsWith : string, caseSensitive : boolean = true) : boolean
    {
        str = EnsureString(str);
        startsWith = EnsureString(startsWith);
        if (!caseSensitive)
        {
            str = str.toLowerCase();
            startsWith = startsWith.toLowerCase();
        }
        let ret = str;
        let pos = ret.indexOf(startsWith);
        return pos == 0;
    }


    /**
     * Returns always a NEW string starting with "startsWith" parameter.
     * If the passed "str" wasn't already starting with "startsWith", this will be prepended.
     * Parameters are always string-ensured.
     *
     * @method EnsureStartsWith
     * @param str
     * @param startsWith
     * @param caseSensitive
     */
    export function EnsureStartsWith(str : any, startsWith : string, caseSensitive : boolean = true) : string
    {
        str = EnsureString(str);
        startsWith = EnsureString(startsWith);

        if (StringStartsWith(str, startsWith, caseSensitive))
            return str;
        return startsWith + str;
    }


    /**
     * Encodes the passed string to BASE64.
     *
     * The "Unicode Problem"
     * Since DOMStrings are 16-bit-encoded strings, in most browsers calling window.btoa on a Unicode string will cause a Character Out Of Range exception if a character exceeds the range of a 8-bit byte (0x00~0xFF). There are two possible methods to solve this problem:
     * - the first one is to escape the whole string (with UTF-8, see encodeURIComponent) and then encode it;
     * - the second one is to convert the UTF-16 DOMString to an UTF-8 array of characters and then encode it.
     * This is the FIRST method implementation
     *
     * @method Base64EncodeUnicode
     */
    export function Base64EncodeUnicode(str : string) : string
    {
        if (isNode)
        {
            return Buffer.from(str).toString('base64');
        }
        else
        {
            // first we use encodeURIComponent to get percent-encoded UTF-8,
            // then we convert the percent encodings into raw bytes which
            // can be fed into btoa.
            return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
                function toSolidBytes(match, p1)
                {
                    return String.fromCharCode(parseFloat('0x' + p1));
                }));
        }
    }


    /**
     * Decodes the passed BASE64 string.
     *
     * The "Unicode Problem"
     * Since DOMStrings are 16-bit-encoded strings, in most browsers calling window.btoa on a Unicode string will cause a Character Out Of Range exception if a character exceeds the range of a 8-bit byte (0x00~0xFF). There are two possible methods to solve this problem:
     * - the first one is to escape the whole string (with UTF-8, see encodeURIComponent) and then encode it;
     * - the second one is to convert the UTF-16 DOMString to an UTF-8 array of characters and then encode it.
     * This is the FIRST method implementation
     *
     * @method Base64DecodeUnicode
     */
    export function Base64DecodeUnicode(str : string) : string
    {
        if (isNode)
        {
            return Buffer.from(str, 'base64').toString('utf-8');
        }
        else
        {
            // Going backwards: from bytestream, to percent-encoding, to original string.
            return decodeURIComponent(atob(str).split('').map(function(c)
            {
                return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
            }).join(''));
        }
    }



    /**
     * Base type to create a new JS ENUM structure (useful to convert its values numeric <=> string   and for FLAGS)
     *
     * @class EnumBaseType
     */
    export class EnumBaseType
    {
        /**
         * Creates a new JS ENUM structure specifying its values and whether it's a FLAG
         *
         * ex:
         *      let MY_ENUM = new EnumBaseType();
         *      MY_ENUM["Prop1"] = 1;
         *      MY_ENUM["Prop2"] = 2;
         *      console.log(MY_ENUM.GetStringValue(2));     //logs "Prop2"
         *
         * or:
         *      let MY_ENUM = new EnumBaseType({Prop1 : 1, Prop2 : 2}, true);
         *      console.log(MY_ENUM.GetStringValue(3));     //logs "Prop1,Prop2"
         *
         * @method constructor
         * @param {object} values An Object with the definition of the ENUM values as key-value
         * @param {boolean} [EnumIsFlag] Indicates if this type is a FLAG (OR-able values). Default = FALSE
         */
        constructor(values : { [Id : string] : number } = null, public EnumIsFlag : boolean = false)
        {
            //TODO: with TypeScript >= 2 this property can be specified as READONLY

            if (__.IsNotNull(values))
            {
                for (let p in values)
                {
                    if (!values.hasOwnProperty(p))
                        continue;
                    this[p] = values[p];
                }
            }
        }


        /**
         * Returns the string (or LIST of strings separated by "," if EnumIsFlag == true) represented by the passed numeric value
         *
         * @method GetStringValue
         * @param {number} numericEnumValue
         * @returns {string}
         */
        public GetStringValue(numericEnumValue : number) : string
        {
            let ret = "";
            let me = <any>this;

            for (let key in me)
            {
                if (!me.hasOwnProperty(key) || !__.IsNumber(me[key]))
                    continue;

                if (this.EnumIsFlag)
                {
                    if (me[key] == 0 || (numericEnumValue & me[key]))
                        ret = ret + (ret == "" ? "" : ",") + key;
                }
                else
                {
                    if (numericEnumValue == me[key])
                        return key;
                }
            }
            return ret;
        }


        /**
         * Given a FLAG number and its ENUM definition returns an array of strings containing the
         * values represented by the flag number.
         * Useful for Kendo MVVM checkboxes
         *
         * @param flag The number (ex. 129)
         * @param enumType the EnumBaseType of the passed flag
         * @return {Array} ex. ["1", "128"]
         */
        public static ConvertFlagToArray(flag : number, enumType : EnumBaseType) : string[]
        {
            let ret = [];
            let flagStringValues = enumType.GetStringValue(flag);
            flagStringValues.split(",").forEach(function(checked)
            {
                let intValue = enumType[checked];
                if (__.IsNotNull(intValue))
                    ret.push(intValue.toString());
            });

            return ret;
        }


        /**
         * Given an array of strings containing the values represented by the flag
         * returns the computed flag number value (OR)
         * Useful for Kendo MVVM checkboxes
         *
         * @param flags The array of string values (ex. ["1", "128"] )
         * @param enumType the EnumBaseType of the passed flag
         * @return {Array} The computed flag number (ex. 129)
         */
        public static ConvertArrayToFlag(flags : string[], enumType : EnumBaseType) : number
        {
            let ret = 0;
            for (let enumTxt in enumType)
            {
                if (!enumType.hasOwnProperty(enumTxt) || !__.IsNumber(enumType[enumTxt]))
                    continue;

                if (__.SearchInArray(flags, enumType[enumTxt].toString()) >= 0)
                    ret = ret | enumType[enumTxt];
            }

            return ret;
        }

    }



    export module Guid
    {
        export function New()
        {
            if (IsNull(uuidModuleInstance))
            {
                if (isNode)
                    uuidModuleInstance = require('node-uuid');
                else
                    uuidModuleInstance = window["uuid"];
            }

            return uuidModuleInstance.v4().toUpperCase();
        }


        /**
         * Returns a NEW GUID without dashes
         *
         * @method NewShort
         * @return {string}
         */
        export function NewShort() : string
        {
            return New().replace(/-/gi, "");
        }


        export function Empty()
        {
            return "00000000-0000-0000-0000-000000000000";
        }


        /**
         * Returns the short EMPTY GUID (without dashes)
         *
         * @method EmptyShort
         * @return {string}
         */
        export function EmptyShort() : string
        {
            return "00000000000000000000000000000000";
        }


        /**
         * Checks if the value is a string that contain a valid NON-EMPTY Guid (NORMAL or SHORT)
         * NO case-sensitive!
         *
         * @param value {string} the string to check
         * @method IsReady
         * @return {boolean}
         */
        export function IsReady(value : string) : boolean
        {
            return (IsGuid(value) && value != Empty()) || (IsShortGuid(value) && value != EmptyShort());
        }


        /**
         * Returns TRUE if obj SEEMS to be a GUID
         *
         * @method IsGuid
         * @param obj What to check
         * @return {boolean}
         */
        export function IsGuid(obj) : obj is string
        {
            return IsString(obj) &&
                obj.length == 36 &&
                obj.replace(/\s/g, "").length == 36 &&     //with NO spaces or NEW LINES
                obj.replace(/-/g, "").length == 32;         //with 4 dashes
        }


        /**
         * Returns TRUE if obj SEEMS to be a SHORT GUID (without dashes)
         *
         * @method IsGuid
         * @param obj What to check
         * @return {boolean}
         */
        export function IsShortGuid(obj) : obj is string
        {
            return IsString(obj) &&
                obj.length == 32 &&
                obj.replace(/\s/g, "").length == 32;     //with NO spaces or NEW LINES
        }


        /**
         * Checks if 2 Guids are equal (NORMAL or SHORT, case insensitive)
         *
         * @method Equals
         * @return {boolean}
         */
        export function Equals(value1 : string, value2 : string) : boolean
        {
            return IsNotEmptyString(value1) && IsNotEmptyString(value2) && value1.toLowerCase() == value2.toLowerCase();
        }

    }



    /**
     * Following the current culture format (GetCurrentLanguage()) returns a NUMBER object or NULL if the string could not be parsed.
     *
     * @method ParseNumber
     * @param {string} str The string to parse
     * @return {number}
     */
    export function ParseNumber(str : string) : number
    {
        return parseFloat(str);
    }


    /**
     * Returns TRUE if the passed string represents the "true" boolean value (case INSENSITIVE check), FALSE otherwise
     *
     * @method ParseBoolean
     * @param {string} str The string to parse
     * @return {boolean}
     */
    export function ParseBoolean(str : string) : boolean
    {
        str = EnsureString(str, "false").trim().toLowerCase();
        return str == "true";
    }


    /**
     * Returns TRUE if the passed string is EMPTY or is an URL ( starts with http(s):// or ftp(s):// )
     *
     * @method StringIsUrl
     * @param {string} str The string to check
     * @return {boolean}
     */
    export function StringIsUrl(str : string) : boolean
    {
        return IsEmptyString(str) || (/^(https?|ftps?):\/\//gi.test(str));
    }



    export function UrlToObject(url? : string) : any
    {
        let urlParams = {};
        let e,
            a = /\+/g,  // Regex for replacing addition symbol with a space
            r = /([^&=]+)=?([^&]*)/g,
            d = function(s) { return decodeURIComponent(s.replace(a, " ")); },
            q = IsNull(url) || typeof url != "string" ? window.location.search.substring(1) : url;

        while (e = r.exec(q))
            urlParams[d(e[1])] = d(e[2]);
        return urlParams;
    }

    /**
     * Returns the CURRENT LANGUAGE
     *
     * @method GetCurrentLanguage
     * @return {string}
     */
    export function GetCurrentLanguage() : string
    {
        if (!IsNullOrEmpty(currentLanguage))
        {
            return currentLanguage;
        }

        //never set with SetCurrentLanguage. Use KENDO if available
        if (IsNotNull(window["kendo"]) && IsFunction(window["kendo"].culture)) //must be set BEFORE document.ready
            return window["kendo"].culture().name;

        //never set with SetCurrentLanguage and KENDO NOT available. Try reading the DOM
        if (IsNotNull(window) && IsNotNull(window["$"]))
            return (<any>(window["$"]("html"))).attr("lang");

        return null;
    }


    /**
     * Changes the CURRENT LANGUAGE
     *
     * @method SetCurrentLanguage
     * @param val The value to set
     */
    export function SetCurrentLanguage(val : string) : void
    {
        currentLanguage = val;

        if (isNode)
            return;

        //update Kendo culture (if possible)
        if (IsNotNull(window["kendo"]) && IsFunction(window["kendo"].culture)) //must be set BEFORE document.ready
            window["kendo"].culture(val);

        //update the DOM (if possible)
        if (IsNotNull(window) && IsNotNull(window["$"]))
            (<any>(window["$"]("html"))).attr("lang", val);
    }


    /**
     * Given a LocalizedStringModel returns the localized value for a specific language
     * (or the CURRENT one if not specified)
     *
     * @method GetLocalizedValue
     * @return {string}
     */
    export function GetLocalizedValue(localizedStringModel : { Values : any }, language? : string) : string
    {
        let ret = "";

        //no language specified? Defaults to CURRENT one
        if (IsEmptyString(language))
            language = GetCurrentLanguage().toLowerCase();
        else
            language = language.toLowerCase();

        //wrong object passed?
        if (IsNull(localizedStringModel) || IsNull(localizedStringModel.Values))
            return ret;

        //search the value for the specified language DISCARDING language CASE
        for (let k in localizedStringModel.Values)
        {
            if (!localizedStringModel.Values.hasOwnProperty(k))
                continue;
            if (k.toLowerCase() == language)
            {
                ret = localizedStringModel.Values[k];
                break;
            }
        }

        return ret;
    }


    /**
     * Given an array of objects returns an object with all grouped keys mapping to the array of related items.
     *
     * @param arr {array} The array to cycle
     * @param key {string} The property to use to group items
     * @param keysComparer {Function} If specified, a custom key comparer (ex: case inensitive match?)
     * @return object
     * @method GroupBy<T>
     */
    export function GroupBy<T>(arr : T[], key : string, keysComparer? : (a : string, b : string) => boolean) : { [id : string] : T[] }
    {
        if (!__.IsFunction(keysComparer))
            keysComparer = function(a, b) { return a == b; };

        let ret = {};
        arr.reduce((lastRet, currVal, currIdx) =>
        {
            if (__.IsNull(currVal))
                return lastRet;

            let currKey = __.EnsureString(currVal[key]);
            let keyAlreadyPresent = false;
            for (let presentKey in lastRet)
            {
                if (!lastRet.hasOwnProperty(presentKey))
                    continue;

                if (keysComparer(presentKey, currKey))
                {
                    keyAlreadyPresent = true;
                    currKey = presentKey;       //the comparer says they're equal, BUT COULD NOT BE! Use the firstly added
                    break;
                }
            }

            if (keyAlreadyPresent)
            {
                lastRet[currKey].push(currVal); //this key has already been added: ADD this item
            }
            else
            {
                lastRet[currKey] = [currVal];   //this key has never been added: create the array with this item
            }
            return lastRet;
        }, ret);

        return ret;
    }


    /**
     * Generates a dummy integer HASH code representing the passed string using a dummy custom hashing function
     *
     * @param toHash {string} The string to be hashed
     * @return number
     * @method HashCode
     */
    export function HashCode(toHash : string) : number
    {
        let hash = 0, i, chr, len;
        if (!__.IsString(toHash) || __.IsEmptyString(toHash))
            return hash;

        for (i = 0, len = toHash.length; i < len; i++)
        {
            chr = toHash.charCodeAt(i);
            hash = ((hash << 5) - hash) + (chr << 11);
        }

        return hash | 0; // Convert to 32bit integer;
    }


    /**
     * Returns an empty ID for a new "person" in BLOX URN format
     *
     * @return {string}
     */
    // export function EmptyUrn() : string
    // {
    //     return "URN:BLOX:empty" + $.Guid.Empty();
    // }


    /**
     * Adds "search" function to standard Array object.
     * Does the same than "indexOf", but this is not supported by IE < 9
     * Can search anything: strings, objects, numbers, nulls, ....
     *
     * @method SearchInArray<T>
     *
     * @param {Array} Arr               The array to search into
     * @param {T} toSearch              [OBJ, FUN] The object to search throughout the array
     *                                  or a callback function receiving the element and returning a boolean value stating if the search should terminate
     * @param {boolean} [caseSensitive] (defaults to true): if true AND ToSearch is a string then checks case
     * @param {number} [from]           (defaults to 0): the starting index of the search in the array
     *
     * @return {number} The POSITION index of the searched element
     */
    export function SearchInArray<T>(Arr : T[],
                                     toSearch : T | ((T) => boolean),
                                     caseSensitive : boolean = true,
                                     from : number           = 0) : number
    {
        let len = Arr.length;

        from = Number(from) || 0;

        from = (from < 0)
            ? Math.ceil(from)
            : Math.floor(from);
        if (from < 0)
            from += len;

        if (typeof toSearch == "function")
        {
            //ToSearch is a NOT NULL Callback
            for (; from < len; from++)
            {
                if (from in Arr &&
                    (<(T) => boolean>toSearch)(Arr[from]))
                    return from;
            }
        }
        else if (typeof toSearch == "string")
        {
            //ToSearch is a NOT NULL STRING

            if (caseSensitive)
            {
                //NORMAL STRING comparison
                for (; from < len; from++)
                {
                    if (from in Arr &&
                        Arr[from] == toSearch)
                        return from;
                }
            }
            else
            {
                //CASE INSENSITIVE COMPARISON
                let ToSearchLower = EnsureString(toSearch).toString().toLowerCase();
                for (; from < len; from++)
                {
                    if (from in Arr &&
                        Arr[from] != null &&
                        EnsureString(Arr[from]).toString().toLowerCase() == ToSearchLower)
                        return from;
                }
            }
        }
        else
        {
            //NORMAL OBJECT (null?) COMPARISON
            for (; from < len; from++)
            {
                if (from in Arr && Arr[from] == toSearch)
                    return from;
            }
        }

        return -1;
    }


    /**
     * Same as SearchInArray, but returns the found ITEM (not its position in array) if found, or NULL otherwise
     *
     * @method SearchValInArray<T>
     */
    export function SearchValInArray<T>(Arr : T[],
                                        toSearch : T | ((T) => boolean),
                                        caseSensitive : boolean = true,
                                        from : number           = 0) : T
    {
        let pos = SearchInArray(Arr, toSearch, caseSensitive, from);
        if (pos < 0)
            return null;
        return Arr[pos];
    }



    /**
     * Searches the passed array and returns the FIRST ITEM with the specified "Id" field, or "undefined" if not found.
     * Similar to {{#crossLink "__/SearchInArray<T>"}}{{/crossLink}}
     *
     * @method SearchById<T>
     *
     * @param {Array} Arr               The array to search into
     * @param {string} id               The Id value to search
     * @param {boolean} [caseSensitive] (defaults to true): if true AND ToSearch is a string then checks case
     * @param {number} [from]           (defaults to 0): the starting index of the search in the array
     *
     * @returns {T} The FIRST item with the specified "Id" field
     */
    export function SearchById<T extends { Id : string }>(Arr : T[], id : string, caseSensitive : boolean = true, from : number = 0) : T
    {
        if (!caseSensitive)
            id = id.toLowerCase();

        let pos = SearchInArray(Arr, (el : T) =>
        {
            if (IsNull(el) || !IsString(el.Id))
                return false;

            if (caseSensitive)
                return el.Id == id;
            else
                return el.Id.toLowerCase() == id;
        }, true, from);

        return Arr[pos];
    }



    /**
     * Searches the passed array. If the element is found REMOVES it.
     *
     * @param {Array} array The array to search and modify
     * @param {object|function} element The element to search. If is a function it must return bool (TRUE when found)
     * @return {boolean} TRUE if found and removed, FALSE otherwise
     */
    export function RemoveFromArray<T>(array : T[], element : T) : boolean
    {
        let pos = SearchInArray(array, element, false, 0);
        if (pos < 0)
            return false;

        array.splice(pos, 1);
        return true;
    }



    /**
     * Removes the element at position "idx" of the passed "array"
     */
    export function RemoveFromArrayByIdx(array : any[], idx : number) : void
    {
        array.splice(idx, 1);
    }



    /**
     * Removes all elements in the passed array satisfying the passed condition
     *
     * @method RemoveFromArrayByCondition
     * @param {Array} array The array to alter
     * @param {function} condition The function checking if the element satisfies the condition for it to be removed from the array
     * @returns {number} the removed items count
     */
    export function RemoveFromArrayByCondition<T>(array : T[], condition : (element : T) => boolean) : number
    {
        let ret = 0;
        if (!__.IsFunction(condition))
            return ret;

        for (let i = 0; i < array.length; i++)
        {
            if (condition(array[i]))
            {
                array.splice(i, 1);
                i--;
                ret++;
            }
        }

        return ret;
    }



    /**
     * Insert an element into an array at the specified position.
     *
     * @method InsertInArray<T>
     * @param {Array} array The array to alter
     * @param {object|function} element The element to insert
     * @param {number} position The 0-based position where to insert the passed element
     * @return {boolean} TRUE if insertion succeeded, FALSE otherwise
     */
    export function InsertInArray<T>(array : T[], element : T, position : number) : boolean
    {
        if (!__.IsArray(array) || position < 0 || position > array.length)
            return false;

        array.splice(position, 0, element);

        return true;
    }



    /*
     * Return a random string of characters
     *
     * @param {Integer} stringLength    Number of random characters the returned string should contain. Defaults to 16.
     * @param {String}  chars           Available characters to use in a string. Defaults to [A-Za-z0-9]
     * @returns {String}                Generated random string
     *
     */
    export function RandomString(stringLength : number, chars? : string) : string
    {
        let newString = "";

        if (!chars)
        {
            chars = "ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz0123456789";
        }

        if (!stringLength)
        {
            stringLength = 16;
        }

        for (let i = 0; i < stringLength; i = i + 1)
        {
            let randomNumber = Math.floor(Math.random() * chars.length);
            newString += chars.substring(randomNumber, randomNumber + 1);
        }

        return newString;
    }



    /**
     * DEEP CLONING of a "thing" which can be a NATIVE type or an object.
     * The ONLY thing which IS NOT cloned (but copied by reference) are DOM NODES
     *
     * KNOWN ISSUE: objects are cloned calling their constructor WITHOUT ANY PARAMETER!!!!
     *
     * @method CloneObj<T>
     * @param {T} obj The "thing" to clone
     * @return {T} A perfect COPY of the passed "thing"
     */
    export function CloneObj<T>(obj : T) : T
    {
        let i, ret, ret2;
        if (typeof obj === "object")
        {
            if (obj === null) return obj;
            let constructorType = Object.prototype.toString.call(obj);
            if (constructorType === "[object Array]")
            {
                ret = [];
                for (i = 0; i < (<any>obj).length; i++)
                {
                    if (typeof obj[i] === "object")
                    {
                        ret2 = CloneObj(obj[i]);
                    }
                    else
                    {
                        ret2 = obj[i];
                    }
                    ret.push(ret2);
                }
            }
            else if (constructorType === "[object Date]")
            {
                ret = new Date((<any>obj).getTime());
            }
            else if (constructorType.substr(0, 12) === "[object HTML") //F.P. DOM NODES
            {
                ret = obj;
            }
            else if (IsJquery(obj)) //F.P. JQuery objects
            {
                ret = window["$"](obj);
            }
            else
            {
                if (typeof (<any>obj).constructor == "function")
                {
                    try
                    {
                        ret = new (<any>obj).constructor(); //F.P. The ONLY problem here: called with NO PARAMETERS.....
                    }
                    catch (e)
                    {
                        ret = {};
                    }
                }
                else
                    ret = {};

                for (i in <any>obj)
                {
                    if ((<any>obj).hasOwnProperty(i))
                    {
                        if (typeof (obj[i] === "object"))
                        {
                            ret2 = CloneObj(obj[i]);
                        }
                        else
                        {
                            ret2 = obj[i];
                        }
                        ret[i] = ret2;
                    }
                }
            }
        }
        else
        {
            ret = obj;
        }

        return ret;
    }



    /**
     *  Calls a passed callback for each NESTED property of the passed object
     *
     * @method CycleObjectNestedProperties
     * @param obj The object to traverse
     * @param onProp Callback to be called on each property found. Receives the property path, value and parent object. If returns FALSE the traversing is stopped
     * @param callbackOnNestedObjectsToo If TRUE "onProp" get fired also for properties holding nested objects
     */
    export function CycleObjectNestedProperties(obj : object, onProp : (path : string, value : any, parentObject : object) => boolean, callbackOnNestedObjectsToo : boolean = false) : void
    {
        /**
         * Returns FALSE when needed to BLOCK cycling, true otherwise
         */
        function CycleFlatProps(prefix : string, obj : object) : boolean
        {
            if (!__.IsNotNullObject(obj))
                return true;

            let usedPrefix = __.IsEmptyString(prefix) ? "" : prefix + ".";
            for (let k in obj)
            {
                if (!obj.hasOwnProperty(k))
                    continue;

                let val = obj[k];
                if (__.IsNotNullObject(val) && !__.IsNativeType(val))
                {
                    if (callbackOnNestedObjectsToo)
                    {
                        let userRet = onProp(usedPrefix + k, val, obj);
                        if (userRet === false)
                            return false;
                    }
                    let recursiveRet = CycleFlatProps(usedPrefix + k, val);
                    if (recursiveRet === false)
                        return false;
                }
                else
                {
                    let userRet = onProp(usedPrefix + k, val, obj);
                    if (userRet === false)
                        return false;
                }
            }
            return true;
        }

        CycleFlatProps("", obj);
    }



    /**
     * Returns a NEW object with all properties of EXTENSION and the ones in BASE which are not present in EXTENSION.
     * NULL values in extension are not ignored but COPIED (overwrite value in base object), UNDEFINED ones are IGNORED!
     * Nested properties are copied with the same logic
     * IF MORE than 2 arguments are passed all RIGHT ones are merged sequentially in the LEFT ones
     *
     * Ex.          MergeObj(A, B, C, D, E)
     * Expands to:  let AB = MergeObj(A, B)
     *              let ABC = MergeObj(AB, C)
     *              let ABCD = MergeObj(ABC, D)
     *              return MergeObj(ABCD, E)
     *
     * @method MergeAnyObj
     * @param params The objects to be merged
     * @returns {object} The merged result
     */
    export function MergeAnyObj(...params : any[]) : any
    {
        //CASE with a lot of arguments to be merged
        if (arguments.length >= 2)
        {
            let prevObj = arguments[0];
            for (let i = 1; i < arguments.length; i++)
            {
                prevObj = MergeObj(prevObj, arguments[i]);
            }
            return prevObj;
        }
    }


    /**
     * Returns a NEW object with all properties of EXTENSION and the ones in BASE which are not present in EXTENSION.
     * NULL values in extension are not ignored but COPIED (overwrite value in base object), UNDEFINED ones are IGNORED!
     * Nested properties are copied with the same logic
     *
     * Ex.          MergeObj(A, B, C, D, E)
     * Expands to:  let AB = MergeObj(A, B)
     *              let ABC = MergeObj(AB, C)
     *              let ABCD = MergeObj(ABC, D)
     *              return MergeObj(ABCD, E)
     *
     * @returns {object} The merged result
     *
     * @see MergeAnyObj
     * @method MergeObj
     * @param base The BASE object to be extended
     * @param extension The extension to be added to BASE
     */
    export function MergeObj<T1, T2>(base : T1, extension : T2) : T1 & T2
    {
        let ret : any = CloneObj(base);

        for (let k in extension)
        {
            if (!extension.hasOwnProperty(k) ||
                Object.prototype.toString.call(extension[k]) == "[object Undefined]")           //IGNORE properties in extension with value set to UNDEFINED
                continue;

            if (Object.prototype.toString.call(ret[k]) == "[object Object]")
            {
                if (Object.prototype.toString.call(extension[k]) == "[object Object]")
                    ret[k] = MergeObj(ret[k], extension[k]);                                    //OBJECT present in base? MERGE it too!
                else
                    ret[k] = CloneObj(extension[k]);                                            //native type present in base? OVERWRITE it with an exact copy!
            }
            else
                ret[k] = CloneObj(extension[k]); //not present/NULL/or other native types? make an exact copy!
        }

        return ret;
    }



    /**
     * Returns all the KEYS of the passed object as an ARRAY of strings, or NULL if the passed obj IS NOT an object
     *
     * @method ObjKeysToArray
     * @param obj
     * @return {null|string[]}
     */
    export function ObjKeysToArray(obj : object) : string[]
    {
        if (!IsNotNullObject(obj))
            return null;

        let ret = [];
        for (let k in obj)
        {
            if (!obj.hasOwnProperty(k))
                continue;
            ret.push(k);
        }
        return ret;
    }



    /**
     * Returns an property (eventually nested) traversing the passed object.
     * Returns the passed object if NO path is given, UNDEFINED if not found.
     *
     * @method GetObjProperty
     * @param obj The object to be traversed. EX: {prop1 : {prop2 : 33}}
     * @param path The path to the requested property. EX: "prop1.prop2"
     */
    export function GetObjProperty(obj : object, path : string) : any
    {
        if (!IsNotNullObject(obj))
            return null;

        let currObj : any = obj;

        path = __.EnsureString(path);
        if (__.IsEmptyString(path))
            return currObj;

        let segments = path.split(".");
        for (let i = 0; i < segments.length; i++)
        {
            currObj = currObj[segments[i]];
            if (__.IsNull(currObj))
                return currObj;
        }

        return currObj;
    }



    /**
     * Wraps a function with a passed one. Features:
     *
     * - it's possible to run the passed "newFun" BEFORE or AFTER the original one
     * - the original context is preserved
     * - "newFun" will receive an additional parameter with infos and options:
     *   - "returnMyReturn" option: control which return object to be used
     *   - "proceed" option: control whether or not to call the original function
     *   - "argumentsToPass" option: customize the passed arguments to the original function
     *   - "originalReturn" option: the original returned object (if "before" == FALSE)
     *
     * @param newFun [function]
     *   The new function (will receive an additional parameter with infos and options)
     *
     * @param funName [string]
     *   The function name to wrap
     *
     * @param [funContext] [object]
     *   The context object holding the member "funName".
     *   It will be the "this" for both functions.
     *   Defaults to the global "window" object
     *
     * @param [before] [boolean]
     *   If TRUE "newFun" will be called before the original,
     *   otherwise after. Defaults to TRUE
     *
     * @param [callOnDynamicThis] [boolean]
     *   If TRUE "funContext" will be ignored as "this" and both functions
     *   will be run on the "run-time" object "this". Defaults to TRUE
     */
    export function WrapFun(newFun : Function,
                            funName : string,
                            funContext? : any,
                            before? : boolean,
                            callOnDynamicThis? : boolean)
    {
        //default values
        if (IsNull(funContext))
            funContext = window;
        if (IsNull(before))
            before = true;
        if (IsNull(callOnDynamicThis))
            callOnDynamicThis = true;

        let oriFun = funContext[funName];

        funContext[funName] = function()
        {
            let wrapOpt = {
                before          : before,
                proceed         : false,
                argumentsToPass : <string[]>[],
                returnMyReturn  : true
            };
            let newRet, oriRet;

            if (callOnDynamicThis)
                funContext = this;

            //create a new list of arguments (Can't edit directly "arguments" obj..)
            let newArgs = [];
            for (let i = 0; i < arguments.length; i++)
                newArgs.push(arguments[i]);

            if (before)
            {
                wrapOpt["proceed"] = true;
                wrapOpt["argumentsToPass"] = newArgs.slice();                       //clone it!
                wrapOpt["returnMyReturn"] = false;

                newArgs.push(wrapOpt);
                newRet = newFun.apply(funContext, newArgs);                         //call the NEW function with a new final editable Settings argument (WARNING: args can be edited!)
                if (!IsArray(wrapOpt["argumentsToPass"]))                         //could have been deleted...
                    wrapOpt["argumentsToPass"] = [];

                if (wrapOpt.proceed)
                {
                    oriRet = oriFun.apply(funContext, wrapOpt["argumentsToPass"]);  //call the ORIGINAL function
                    if (wrapOpt.returnMyReturn)
                        return newRet;
                    else
                        return oriRet;
                }
                else
                {
                    if (wrapOpt.returnMyReturn)
                        return newRet;
                    else
                        return undefined;
                }
            }
            else
            {
                oriRet = oriFun.apply(funContext, newArgs);       //call the ORIGINAL function (WARNING: args can be edited!)

                wrapOpt["originalReturn"] = oriRet;
                wrapOpt["returnMyReturn"] = true;

                newArgs.push(wrapOpt);
                newRet = newFun.apply(funContext, newArgs);       //call the NEW function with a new final editable Settings argument (WARNING: args can be edited!)

                if (wrapOpt.returnMyReturn)
                    return newRet;
                else
                    return oriRet;
            }
        };

        funContext[funName]["originalFun"] = oriFun;   //remember the original function to be able to UNWRAP it
    }



    /**
     * Reverts to the original a wrapped function created with __.WrapFun(...)
     *
     * @param funName [string] The function name to restore
     * @param [funContext] [object] The context object holding the member "funName". Defaults to the global "window" object
     */
    export function UnwrapFun(funName, funContext)
    {
        if (typeof funContext[funName]["originalFun"] == "function")
        {
            funContext[funName] = funContext[funName]["originalFun"];
        }
    }



    /**
     * Converts an object to another following the passed rules.
     *
     * @method MapObject
     *
     * @param obj The object to convert
     *
     * @param {string[][]} mappings  The conversion rules.
     * It must be an array of 2 strings arrays.
     * The first states the SOURCE property to be copied, the second the DESTINATION property name.
     * Those properties can be NESTED. Ex: "subObject.subProp"
     *
     * @param {boolean} [ignoreMissingProperties = true] If TRUE when a source property is not found
     * then NULL will be returned (situation threaten as an error)
     *
     * @param {boolean} [cloneObjects = false] If TRUE the converted properties will be a CLONE of the source ones.
     * Otherwise a reference to the same object will be created.
     *
     * @return {Object|null} The converted object or NULL upon errors
     */
    export function MapObject(obj, mappings, ignoreMissingProperties, cloneObjects)
    {
        //CHECK INPUT PARAMETERS
        if (IsNull(obj) || !IsArray(mappings))
            return null;
        if (IsNull(ignoreMissingProperties))
            ignoreMissingProperties = true;
        ignoreMissingProperties = !!ignoreMissingProperties;
        if (IsNull(cloneObjects))
            cloneObjects = false;
        cloneObjects = !!cloneObjects;


        let ret = {};

        for (let i = 0; i < mappings.length; i++)
        {
            let mapping = mappings[i];
            if (!IsArray(mapping) || mapping.length != 2 || !IsString(mapping[0]) || !IsString(mapping[1]))   //unexpected input
            {
                Log("MapObject: Invalid mapping #" + i);
                continue;
            }

            let fromProp = mapping[0];
            let toProp = mapping[1];

            if (!(fromProp in obj) && ignoreMissingProperties == false)   //ERROR!
                return null;

            //set the property
            if (cloneObjects)
                ret[toProp] = CloneObj(obj[fromProp]);
            else
                ret[toProp] = obj[fromProp];
        }

        return ret;
    }



    /**
     * Return the STRING representing the passed ENUM value (for a TypeScript ENUM object)
     *
     * @param e The TypeScript ENUM object
     * @param val The integer value of the ENUM
     */
    export function GetEnumKey(e : any, val : number) : string
    {
        for (let k in e)
        {
            if (e[k] == val)
                return k;
        }
        return "???";
    }



    export class FluentParallel
    {
        private functions : Function[] = [];

        constructor(firstFunction? : (onEnd : (result? : any) => void) => any)
        {
            if (__.IsFunction(firstFunction))
                this.functions.push(firstFunction);
        }

        public Parallel(otherFunction : (onEnd : (result? : any) => void) => any) : FluentParallel
        {
            if (__.IsFunction(otherFunction))
                this.functions.push(otherFunction);
            return this;
        }

        public ExecuteAndJoin(joinFunction : (resultsOrderedByPosition : ExecuteAndJoin.Result[], resultsOrderedByTime : ExecuteAndJoin.Result[], timeoutException? : Error) => void) : void
        {
            if (__.IsFunction(joinFunction))
                this.functions.push(joinFunction);
            ExecuteAndJoin(this.functions);
        }
    }


    /**
     * Helper function offering FLUENT syntax for __.ExecuteAndJoin
     *
     * Calls can be joined:
     * __.Parallel(function(onEnd){})
     *   .Parallel(function(onEnd){})
     *   .Parallel(function(onEnd){})
     *   .ExecuteAndJoin(function(resultsOrderedByPosition, resultsOrderedByTime, timeoutException){})
     *
     * @param {(onEnd: (result?: any) => void) => any} firstFunction Eventual first function to add to the list
     */
    export function Parallel(firstFunction? : (onEnd : (result? : any) => void) => any) : FluentParallel
    {
        return new FluentParallel(firstFunction);
    }


    export module ExecuteAndJoin
    {
        export interface Result
        {
            Position : number;
            SuccessfulResult : any;
            Exception : Error;
        }
    }



    /**
     * Executes ALL passed functions meant to be ASYNCHRONOUS passing to them as only argument a "END CALLBACK" to be called
     * with the result to ce collected.
     *
     * The LAST argument is the "JOIN" function: it will be called when EVERY passed function has called its own "END CALLBACK"
     * or has thrown an exception.
     * This "JOIN" function accepts 2 arrays with all results and an eventual Timeout error (if ALL functions did not call their own "END CALLBACK" in time (ExecuteAndJoin.TimeoutMs)):
     *   the FIRST ordered by the passed functions positions,
     *   the SECOND ordered by "END CALLBACK" call time.
     *
     * <b>WARNING</b>: the return values of the passed functions are IGNORED (the captured one is what is passed to the "END CALLBACK")<br>
     * <b>WARNING</b>: in the passed functions don't execute other code after "END CALLBACK" is called, just return.<br>
     * <b>WARNING</b>: if a passed function never calls its "END CALLBACK", then the "JOIN" function will never be called<br>
     * <b>WARNING</b>: multiple calls to an "END CALLBACK" are ignored (only the first one is considered)<br>
     *
     * @example
     *    ExecuteAndJoin(asyncFun1, asyncFun2, joinFun);
     *
     * @method ExecuteAndJoin
     */
    export function ExecuteAndJoin(...params : any[]) : void
    {
        let functions;
        let me = this;

        //check if functions are passed as ARGUMENTS or in an ARRAY
        if (arguments.length == 1 && IsArray(arguments[0]))
        {
            functions = CloneObj(arguments[0]);
        }
        else
        {
            functions = CloneObj(arguments);
        }

        //check input parameters
        if (functions.length == 0)
            throw new Error("ExecuteAndJoin need at least ONE parameter");
        for (let i = 0; i < functions.length; i++)
        {
            if (!IsFunction(functions[i]))
                throw new Error("Argument " + i + " + of ExecuteAndJoin is not a function");
        }

        let resultsOrderedByPosition : ExecuteAndJoin.Result[] = [];
        let resultsOrderedByTime : ExecuteAndJoin.Result[] = [];
        let numReturned = 0;
        let allFuncsReturned = false;
        let joinCalledWithTimeoutException = false;


        let callJoin = function(timeoutException? : Error)
        {
            if (joinCalledWithTimeoutException)
                return;                         //all functions returned but AFTER the TIMEOUT expired. Discard this join call: the one with the exception has already been called.

            clearTimeout(timeoutCheckTimer);
            allFuncsReturned = true;

            setTimeout(function()
            {
                if (__.IsNull(timeoutException))
                    functions[functions.length - 1].call(me, resultsOrderedByPosition, resultsOrderedByTime);
                else
                {
                    joinCalledWithTimeoutException = true;
                    functions[functions.length - 1].call(me, resultsOrderedByPosition, resultsOrderedByTime, timeoutException);
                }
            }, 1);
        };

        if (functions.length == 1)
        {
            //nothing to execute.... join!
            callJoin();
            return;
        }


        //OK, RUN THE ASYNC FUNCTIONS
        //launch the timeout check
        let timeoutCheckTimer = setTimeout(function()
        {
            if (!allFuncsReturned)
            {
                callJoin(new Error("Timeout of " + ExecuteAndJoin.TimeoutMs + "ms expired!"));
            }
        }, ExecuteAndJoin.TimeoutMs);


        //EXECUTE every function
        for (let i = 0; i < functions.length - 1; i++)
        {
            (function()
            {
                let ii = i;
                setTimeout(function()
                {
                    try
                    {
                        functions[ii].call(me, function(result : any)
                        {
                            //function called END CALLBACK!
                            if (IsNotNullObject(resultsOrderedByPosition[ii]))
                                return;     //END CALLBACK called TWICE, discard next calls...

                            //STORE its result
                            resultsOrderedByPosition[ii] = {
                                Position         : ii,
                                SuccessfulResult : result,
                                Exception        : undefined
                            };
                            resultsOrderedByTime[numReturned] = {
                                Position         : ii,
                                SuccessfulResult : result,
                                Exception        : undefined
                            };

                            //check if we have to JOIN
                            numReturned++;
                            if (numReturned == functions.length - 1)
                                callJoin();
                        });
                    }
                    catch (e)
                    {
                        //function threw an exception
                        //STORE its result
                        resultsOrderedByPosition[ii] = {
                            Position         : ii,
                            SuccessfulResult : undefined,
                            Exception        : e
                        };
                        resultsOrderedByTime[numReturned] = {
                            Position         : ii,
                            SuccessfulResult : undefined,
                            Exception        : e
                        };

                        //check if we have to JOIN
                        numReturned++;
                        if (numReturned == functions.length - 1)
                            callJoin();
                    }
                }, 1);
            })();
        }
    }


    export module ExecuteAndJoin
    {
        export let TimeoutMs : number = 5000;
    }



    /**
     *   Returns a function, that, as long as it continues to be invoked, will not
     *   be triggered. So the function will be called ONLY ONCE, after it stops being called for
     *   N milliseconds.
     *   If `immediate` is passed, triggers the function on its FIRST invocation, instead on the timeout expiration.
     *
     * @param func The function to be called only ONCE
     * @param waitForMs The timeout milliseconds
     * @param immediate If TRUE the  `func` is called upon the first invocation
     */
    export function Debounce(func : () => void, waitForMs : number, immediate : boolean = false) : () => void
    {
        let timeout;
        return function()
        {
            let context = this, args = arguments;
            let later = function()
            {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            let callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, waitForMs);
            if (callNow) func.apply(context, args);
        };
    }



    /*##########################################################################
     ######################   GENERAL DATES RELATED METHODS   ##################
     ##########################################################################*/
    export module Dates
    {

        /**
         * Checks that the passed argument is a string in the Microsoft format like "\/Date(1405728000000)\/"
         *
         * @method IsMsFormatStringDate
         * @param msFormatString
         * @return {boolean}
         */
        export function IsMsFormatStringDate(msFormatString : any) : boolean
        {
            return IsString(msFormatString) &&
                (<string>msFormatString).toString().toLowerCase().substr(0, 6) == "/date(";
        }


        /**
         * Checks that the passed argument is a string in the ISO format like "2014-12-30T15:20:00Z"
         *
         * @method IsMsFormatStringDate
         * @param isoFormatString
         * @return {boolean}
         */
        export function IsISOFormatStringDate(isoFormatString : any) : boolean
        {
            let exp = /(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z))|(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z))|(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z))/;

            return IsString(isoFormatString) && exp.test(isoFormatString);
        }


        /**
         * Given an ISO string like "2014-12-30T15:20:00Z" returns the corresponding date
         * CONVERTED to the local time.
         * Has a fallback to manual parsing for older IEs (TODO: without considering the time shift for now...)
         *
         * @method GetDateFromISO
         * @param {string|*} isoString a string like "2014-12-30T15:20:00Z"
         * @return {Date|null} Returns the DATE object or NULL if the string is not valid
         */
        export function GetDateFromISO(isoString : Date | string) : Date
        {
            if (IsDate(isoString))
                return <Date>isoString;

            if (IsNull(isoString) || IsEmptyString(isoString))
                return null;

            let isoDate = new Date(<string>isoString);

            if (IsNotNull(isoDate) && isNaN(isoDate.getTime())) //NOT a valid ISO string
                return null;

            return isoDate;
        }


        /**
         * Given a Microsoft formatted string like "\/Date(1239018869048)\/"  returns the corresponding date
         *
         * @method GetDateFromMsFormat
         * @param {Date | string} msFormatString
         * @returns {Date}
         */
        export function GetDateFromMsFormat(msFormatString : Date | string) : Date
        {
            if (IsDate(msFormatString))
                return <Date>msFormatString;

            if (IsNull(msFormatString) || IsEmptyString(msFormatString))
                return null;

            if (!IsMsFormatStringDate(msFormatString))
            {
                throw new Error("The value passed to GetDateFromMsFormat is NOT a valid Microsoft format date!");
            }

            //WARNING: doesn't support shift like /Date(43434343+100)/
            let number = (<string>msFormatString).toLowerCase().replace("/date(", "").replace(")/", "");
            return new Date(parseInt(number, 10));
        }



        /**
         * Returns a Date object shifted of the current UTC timespan
         * (useful when need to print in the Local form a json UTC DATE received from the server)
         *
         * @method AddUtcOffsetToDate
         * @param date The date to shift
         * @param [editPassedDate] {boolean} If TRUE the passed date object will be modified, otherwise not. DEFAULTS to FALSE
         * @returns {Date} The shifted date object
         */
        export function AddUtcOffsetToDate(date : Date | string, editPassedDate : boolean) : Date
        {
            if (IsNull(date))
                return null;

            if (IsNull(editPassedDate))
                editPassedDate = false;

            date = GetDateFromISO(date); //convert if necessary

            let toEdit : Date;
            if (editPassedDate)
                toEdit = <Date>date;
            else
                toEdit = new Date((<Date>date).getTime()); //CLONE IT!

            toEdit.setMinutes(toEdit.getMinutes() + ((<Date>date).getTimezoneOffset() * (-1)));
            return toEdit;
        }



        /**
         * Returns a Date object shifted BACK of the current UTC timespan
         * (useful when need to send the json date in UTC starting form a Locale DATE)
         *
         * @method SubtractUtcOffsetToDate
         * @param date The date to shift
         * @param [editPassedDate] {boolean} If TRUE the passed date object will be modified, otherwise not. DEFAULTS to FALSE
         * @returns {Date} The shifted date object
         */
        export function SubtractUtcOffsetToDate(date, editPassedDate) : Date
        {
            if (IsNull(date))
                return null;

            if (IsNull(editPassedDate))
                editPassedDate = false;

            date = GetDateFromISO(date); //convert if necessary

            let toEdit : Date;
            if (editPassedDate)
                toEdit = date;
            else
                toEdit = new Date(date.getTime()); //CLONE IT!

            toEdit.setMinutes(toEdit.getMinutes() - (date.getTimezoneOffset() * (-1)));
            return toEdit;
        }



        /**
         * Useful to handle DATE-ONLY objects discarding the time (ex. birthdate!!!) without problems with server.
         *
         * @method DatePartOnly
         * @param {Date} date
         * @returns {Date} A new date without the TIME (00:00)
         */
        export function DatePartOnly(date) : Date
        {
            return new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()));
        }



        /**
         * Returns the difference of LocalTime with UTC in MINUTES
         * (applying the current daylight time saving)
         *
         * @method GetUtcOffset
         * @returns {number}
         */
        export function GetUtcOffset() : number
        {
            return (new Date()).getTimezoneOffset();
        }

        export function FormatDate(date : Date,
                                   showYear : boolean        = true,
                                   showMonth : boolean       = true,
                                   showDay : boolean         = true,
                                   showHour : boolean        = true,
                                   showMinute : boolean      = true,
                                   showSecond : boolean      = true,
                                   showMillisecond : boolean = true) : string
        {
            let ret = "";
            if (showDay)
                ret += Format("%02d", date.getDate());
            if (showMonth)
                ret += (__.IsEmptyString(ret) ? "" : "/") + Format("%02d", date.getMonth() + 1);
            if (showYear)
                ret += (__.IsEmptyString(ret) ? "" : "/") + Format("%d", date.getFullYear());

            if (!__.IsEmptyString(ret) && (showHour || showMinute || showSecond || showMillisecond))
                ret += " ";

            if (showHour)
                ret += Format("%02d", date.getHours());
            if (showMinute)
                ret += (showHour ? ":" : "") + Format("%02d", date.getMinutes());
            if (showSecond)
                ret += (showHour || showMinute ? ":" : "") + Format("%02d", date.getSeconds());
            if (showMillisecond)
                ret += (showHour || showMinute || showSecond ? "." : "") + Format("%03d", date.getMilliseconds());

            return ret;
        }
    }

}

