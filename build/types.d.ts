export interface IIdModel {
    Id: string;
}
export interface INameModel {
    Name: string;
}
export interface IIdNameModel extends IIdModel, INameModel {
}
export interface IPagedData<T> {
    CurrentPage: number;
    Items: T[];
    ItemsPerPage: number;
    TotalItems: number;
    TotalPages: number;
}
