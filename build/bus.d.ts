export declare module IronLibsBus {
    enum BusRemoteMessageType {
        System = 0,
        EventSent = 1,
        AnswerToEvent = 2
    }
    interface IBusRemoteMessage {
        readonly Type: BusRemoteMessageType;
        readonly Data: IBusEvent | IPublishResult[];
        readonly Id: string;
        readonly OnAnswer?: (response: IPublishResult[]) => void;
    }
    export interface IConnectionPermissions {
        Connection: IRemoteConnection;
        EventsCanReceiveFrom: string[];
        EventsCanForwardTo: string[];
    }
    export interface IRemoteConnection {
        GetUnansweredMessages(): IBusRemoteMessage[];
        GetWs(): WebSocket | any;
        GetPeerAddress(): string;
        GetPeerBusName(): string;
        SendMessage(msg: IBusRemoteMessage): boolean;
        Close(): boolean;
        IsClosed(): boolean;
        GetId(): string;
    }
    export class Bus {
        private readonly _opt;
        constructor(opt?: IBusOptions);
        /**
         *
         * @param subscriptionString The "subscribeTo" string (with eventual wildcards)
         * Examples:
         *      "**"                    matches all
         *      "*"                     matches "MySingle"
         *      "App:MainOperation"     matches none
         *      "App:MainOperation:*"   matches "App:MainOperation:Minimize"
         *      "App:MainOperation:**"  matches "App:MainOperation:Minimize" and "App:MainOperation:Exit:Fatal"
         *      "App:*:Minimize"        matches "App:MainOperation:Minimize" and "App:Project:Minimize"
         *      "**:MasterLog"          matches "App:Project:Render:MasterLog"
         *
         * @param eventName The event name to match.
         * * Examples:
         *      "App:MainOperation:Minimize"
         *      "App:MainOperation:Exit:Fatal"
         *      "App:Project:Minimize"
         *      "App:Project:Render:MasterLog"
         *      "MySingle"
         */
        protected MatchesEventName(subscriptionString: string, eventName: string): boolean;
        protected static IsSubscription(subscription: ISubscription): boolean;
        protected static IsEvent(evt: IBusEvent): boolean;
        /**
         * Returns a copy of current options (not modifiable)
         */
        GetOptions(): IBusOptions;
        GetMatchingSubscriptionsForEventName(evtName: string): ISubscription[];
        GetMatchingSubscriptionsCountForEventName(evtName: string): number;
        GetSubscriptionsCount(): number;
        /**
         * Returns a COPY of registered subscriptions
         */
        GetSubscriptions(): ISubscription[];
        GetRemoteConnections(): IRemoteConnection[];
        Subscribe(subscription: ISubscription): boolean;
        /**
         * Returns the number of removed subscriptions
         */
        RemoveSubscription(subscription: ISubscription): number;
        /**
         * Returns the number of removed subscriptions
         */
        RemoveSubscriptionBySubscribeTo(subscribeTo: string): number;
        /**
         * @returns {number} Returns the number of removed subscriptions
         */
        RemoveSubscriptionByCallback(callback: () => void): number;
        /**
         *
         * @returns Returns LOCAL subscriptions matching the event name which will be fired.
         * Connection's belonging subscriptions are ignored from this (sync) return
         *
         * @param evt
         * @param onLocalSubscriptionEnded
         * @param onAllSubscribersEnded
         * WARNING: the number of results is:    # local subscriptions + [FOR EACH remote connections ] # remote subscriptions
         * considering only subscriptions matching the event name AND connections accepted by permissions
         *
         * @param forwardOnlyToTheseConnections
         * @param dontForwardToTheseConnections
         */
        Publish(evt: IBusEvent, onLocalSubscriptionEnded?: (firingEvent: IBusEvent, endedSubscription: ISubscription, result: any, asyncExecution: boolean) => void, onAllSubscribersEnded?: (firingEvent: IBusEvent, publishResults: IPublishResult[], publishTimeout?: Error) => void, forwardOnlyToTheseConnections?: IRemoteConnection[], dontForwardToTheseConnections?: IRemoteConnection[]): ISubscription[];
        ListenForRemoteConnection(port: number, onListening?: (socketId: string, err: Error) => void, onConnection?: (conn: IRemoteConnection) => void): void;
        /**
         * Makes ALL server sockets stop listening
         * If a socketId is passed (received in callback of ListenForRemoteConnection) only that one will be closed.
         */
        StopListening(socketId?: string): void;
        CloseAllConnections(): void;
        RemoteConnectToBusServer(url: string, onEnd?: (conn: IRemoteConnection, err: Error) => void): void;
        GetAllConnectionsPermissions(): {
            [id: string]: IConnectionPermissions;
        };
        GetConnectionPermissions(conn: IRemoteConnection): IConnectionPermissions;
        UpdateConnectionsPermissions(): void;
    }
    export interface IPublishResult {
        SubscriberName: string;
        FromConnection?: IRemoteConnection;
        SuccessfulResult: any;
    }
    export interface IBusEvent {
        /**
         * The namespaced event identification string to which subscribers subscribe to.
         * Ex:  "App:Main:Init"
         */
        readonly Name: string;
        /**
         * Eventual data associated to the event
         */
        readonly Payload: any;
        /**
         * Eventual identification string representing who is firing the event
         */
        readonly FiredBy: string;
        readonly FiredAt: Date;
    }
    export class BusEvent implements IBusEvent {
        readonly Name: string;
        readonly Payload: any;
        readonly FiredBy: string;
        constructor(Name: string, Payload?: any, FiredBy?: string);
        readonly FiredAt: Date;
    }
    export interface ISubscriptionExecutionContext {
        FiringEvent: IBusEvent;
        Async: boolean;
        AsyncEnded: (result: any) => void;
    }
    export interface ISubscription {
        Callback: (context: ISubscriptionExecutionContext) => any;
        SubscribeTo: string;
        SubscriberName?: string;
    }
    export interface IBusOptions {
        Name?: string;
        LoggerInfo?: (msg: string, bus: Bus) => void;
        LoggerDebug?: (msg: string, bus: Bus) => void;
        LoggerError?: (msg: string, bus: Bus) => void;
        NamespacesSeparator?: string;
        /**
         * Called when a new connection is received. Informs the bus server about the events forwarding policies with the peer.
         * WARNING: if not specified the default permissions are the most permissive!
         *
         * @param fromBusName The belonging bus of the connection for which the permissions are requested
         * @param conn The connection for which the permissions are requested
         * @param setPermissions Function to be called to set permissions (async)
         */
        OnRequestPermissionsForConnection?: (fromBusName: string, conn: IRemoteConnection, setPermissions: (permissions: IConnectionPermissions) => void) => void;
    }
    export function GetDefault(): Bus;
    export function CreateDefault(opt?: IBusOptions): Bus;
    export function CreateNew(opt?: IBusOptions): Bus;
    export {};
}
