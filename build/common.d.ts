export declare module IronLibsCommon {
    /**
     * Changes the actual DEBUG MODE
     *
     * @method SetDebugMode
     * @param val The value to set
     */
    function SetDebugMode(val: boolean): void;
    /**
     * Returns TRUE if application is configured to be in DEBUG MODE
     *
     * @method IsDebugMode
     * @return {boolean}
     */
    function IsDebugMode(): boolean;
    /**
     * log to console one or MORE passed arguments
     */
    function Log(...params: any[]): void;
    function LogFatal(...params: any[]): void;
    function LogError(...params: any[]): void;
    function LogWarn(...params: any[]): void;
    function LogDebug(...params: any[]): void;
    /**
     * Formats a string with N parameters of any type.
     * Wrapper of sprintf
     *
     * @param formatString
     * @param params
     * @returns {string} the formatted string
     */
    function Format(formatString: string, ...params: any[]): string;
    /**
     * Returns the most suitable size format for the given number
     *
     * @method FormatFileSize
     * @param {number|string} fileSize  The number of BYTES for format
     * @param {boolean} shortForm  If TRUE returns the 2-characters representation
     * @return {string}
     */
    function FormatFileSize(fileSize: number | string, shortForm?: boolean): string;
    /**
     * Returns TRUE if obj is NULL or UNDEFINED
     *
     * @method IsNull
     * @param obj What to check
     * @return {boolean}
     */
    function IsNull(obj: any): obj is null;
    /**
     * Returns TRUE if obj isn't NULL or UNDEFINED
     *
     * @method IsNotNull
     * @param obj What to check
     * @return {boolean}
     */
    function IsNotNull(obj: any): boolean;
    /**
     * Returns TRUE if obj IS an object and isn't NULL or UNDEFINED
     *
     * @method IsNotNullObject
     * @param obj What to check
     * @return {boolean}
     */
    function IsNotNullObject(obj: any): boolean;
    /**
     * Returns TRUE if obj is a FUNCTION
     *
     * @method IsFunction
     * @param obj What to check
     * @return {boolean}
     */
    function IsFunction(obj: any): obj is Function;
    /**
     * Returns TRUE if obj is a NUMBER
     *
     * @method IsNumber
     * @param obj What to check
     * @return {boolean}
     */
    function IsNumber(obj: any): obj is number;
    /**
     * Returns TRUE if obj is a Date instance
     *
     * @method IsDate
     * @param obj What to check
     * @return {boolean}
     */
    function IsDate(obj: any): obj is Date;
    enum RoundingMethod {
        FLOOR = 0,
        ROUND = 1,
        NO_ROUNDING = 2
    }
    /**
     * Returns a number with precision clamped to a fixed resolution
     * Ex: LimitNumberPrecision(1.12345678, 3) == 1.123
     *
     * ATTENTION: JavaScript is tricky/buggy! Here some examples:
     *
     * 2.05*100 == 204.99999999999997  not 205  so
     *
     * LimitNumberPrecision(2.05, 2, __.RoundingMethod.FLOOR) == 2.04
     *
     * LimitNumberPrecision(2.05, 2, __.RoundingMethod.NO_ROUNDING) == 2.05
     *
     * @param num The float number to limit
     * @param precision the number of DIGITS after "."
     * @param method The method used to "cut" digits: Math.round, Math.floor or basic extra digits removal with no rounding
     *
     */
    function LimitNumberPrecision(num: number, precision: number, method?: RoundingMethod): number;
    /**
     * Returns TRUE if obj is a native BOOLEAN instance
     *
     * @method IsBoolean
     * @param obj What to check
     * @return {boolean}
     */
    function IsBoolean(obj: any): obj is boolean;
    /**
     * Returns TRUE if obj is a NOT-NULL NUMBER, BOOLEAN, DATE or STRING
     *
     * @method IsNativeType
     * @param obj What to check
     * @return {boolean}
     */
    function IsNativeType(obj: any): obj is (number | boolean | string | Date);
    /**
     * Returns TRUE if obj is an ARRAY
     *
     * @method IsArray
     * @param obj What to check
     * @return {boolean}
     */
    function IsArray(obj: any): obj is any[];
    /**
     * Returns TRUE if obj is a STRING
     *
     * @method IsString
     * @param obj What to check
     * @return {boolean}
     */
    function IsString(obj: any): obj is string;
    /**
     * Returns TRUE if obj is NULL or an EMPTY STRING
     *
     * @method IsEmptyString
     * @param obj What to check
     * @return {boolean}
     */
    function IsEmptyString(obj: any): obj is "";
    /**
     * Returns TRUE if obj is NULL or an EMPTY OBJECT (without any property)
     *
     * @method IsEmptyObject
     * @param obj What to check
     * @return {boolean}
     */
    function IsEmptyObject(obj: any): obj is Object;
    /**
     * Returns TRUE if obj is a STRING and is not empty
     * (WARNING: can have only empty spaces! To check them use IsNotWhiteSpace)
     *
     * @param {string|*} obj
     * @return {boolean}
     */
    function IsNotEmptyString(obj: any): boolean;
    /**
     * Returns TRUE if obj is NULL, an EMPTY STRING or an EMPTY ARRAY
     *
     * @method IsNullOrEmpty
     * @param obj What to check
     * @return {boolean}
     */
    function IsNullOrEmpty(obj: string | any): obj is (null | "");
    /**
     * Returns TRUE if obj is an ARRAY AND has at least one element
     * @return {boolean}
     */
    function IsNotEmptyArray(obj: any): boolean;
    /**
     * Returns TRUE if the passed object is a jquery-wrapped object.
     * (automatic check of NOT-NULL)
     *
     * @method IsJquery
     * @param obj What to check
     * @return {boolean}
     */
    function IsJquery(obj: any): boolean;
    /**
     * Returns TRUE if the passed object is a string representing a valid EMAIL address.
     * (automatic check of NOT-NULL)
     *
     * @method IsEmail
     * @param obj What to check
     * @return {boolean}
     */
    function IsEmail(obj: any): obj is string;
    /**
     * Returns the passed email address:
     * - lower case domain (RFC 1035 "Name servers and resolvers must compare [domains] in a case-insensitive manner")
     * and for some domains (which consider the emails equivalent):
     * - without dots
     * - without substring after "+"
     *
     * @method NormalizeEmail
     */
    function NormalizeEmail(eMail: string): string;
    /**
     * Returns ALWAYS a string: the representation of the passed object:
     * EMPTY string upon NULL, or toString() upon OBJECT
     *
     * @param {*} str
     * @param {string} defaultValue
     * @return {string}
     */
    function EnsureString(str: any, defaultValue?: string): string;
    /**
     * Returns ALWAYS something NOT NULL (which can be specified as defaultValue).
     * Similar to {{#crossLink "__/EnsureString"}}{{/crossLink}} but for objects.
     *
     * @method EnsureNotNull
     * @param obj Anything to be checked
     * @param {string} defaultValue What to return in case str is NULL. Defaults to {}
     * @return {object}
     */
    function EnsureNotNull(obj: any, defaultValue?: any): any;
    /**
     * If "str" ends with "endsWith" returns TRUE, FALSE otherwise.
     * Parameters are always string-ensured.
     *
     * @method StringEndsWith
     * @param str The string to check
     * @param endsWith The ending to check
     * @param caseSensitive CASE check, defaults to TRUE
     */
    function StringEndsWith(str: any, endsWith: string, caseSensitive?: boolean): boolean;
    /**
     * Returns always a NEW string ending with "endsWith" parameter.
     * If the passed "str" wasn't already ending with "endsWith", this will be appended.
     * Parameters are always string-ensured.
     *
     * @method EnsureEndsWith
     * @param str
     * @param endsWith
     * @param caseSensitive
     */
    function EnsureEndsWith(str: any, endsWith: string, caseSensitive?: boolean): string;
    /**
     * If "str" starts with "startsWith" returns TRUE, FALSE otherwise.
     * Parameters are always string-ensured.
     *
     * @method StringStartsWith
     * @param str The string to check
     * @param startsWith The starting string to check
     * @param caseSensitive CASE check, defaults to TRUE
     */
    function StringStartsWith(str: any, startsWith: string, caseSensitive?: boolean): boolean;
    /**
     * Returns always a NEW string starting with "startsWith" parameter.
     * If the passed "str" wasn't already starting with "startsWith", this will be prepended.
     * Parameters are always string-ensured.
     *
     * @method EnsureStartsWith
     * @param str
     * @param startsWith
     * @param caseSensitive
     */
    function EnsureStartsWith(str: any, startsWith: string, caseSensitive?: boolean): string;
    /**
     * Encodes the passed string to BASE64.
     *
     * The "Unicode Problem"
     * Since DOMStrings are 16-bit-encoded strings, in most browsers calling window.btoa on a Unicode string will cause a Character Out Of Range exception if a character exceeds the range of a 8-bit byte (0x00~0xFF). There are two possible methods to solve this problem:
     * - the first one is to escape the whole string (with UTF-8, see encodeURIComponent) and then encode it;
     * - the second one is to convert the UTF-16 DOMString to an UTF-8 array of characters and then encode it.
     * This is the FIRST method implementation
     *
     * @method Base64EncodeUnicode
     */
    function Base64EncodeUnicode(str: string): string;
    /**
     * Decodes the passed BASE64 string.
     *
     * The "Unicode Problem"
     * Since DOMStrings are 16-bit-encoded strings, in most browsers calling window.btoa on a Unicode string will cause a Character Out Of Range exception if a character exceeds the range of a 8-bit byte (0x00~0xFF). There are two possible methods to solve this problem:
     * - the first one is to escape the whole string (with UTF-8, see encodeURIComponent) and then encode it;
     * - the second one is to convert the UTF-16 DOMString to an UTF-8 array of characters and then encode it.
     * This is the FIRST method implementation
     *
     * @method Base64DecodeUnicode
     */
    function Base64DecodeUnicode(str: string): string;
    /**
     * Base type to create a new JS ENUM structure (useful to convert its values numeric <=> string   and for FLAGS)
     *
     * @class EnumBaseType
     */
    class EnumBaseType {
        EnumIsFlag: boolean;
        /**
         * Creates a new JS ENUM structure specifying its values and whether it's a FLAG
         *
         * ex:
         *      let MY_ENUM = new EnumBaseType();
         *      MY_ENUM["Prop1"] = 1;
         *      MY_ENUM["Prop2"] = 2;
         *      console.log(MY_ENUM.GetStringValue(2));     //logs "Prop2"
         *
         * or:
         *      let MY_ENUM = new EnumBaseType({Prop1 : 1, Prop2 : 2}, true);
         *      console.log(MY_ENUM.GetStringValue(3));     //logs "Prop1,Prop2"
         *
         * @method constructor
         * @param {object} values An Object with the definition of the ENUM values as key-value
         * @param {boolean} [EnumIsFlag] Indicates if this type is a FLAG (OR-able values). Default = FALSE
         */
        constructor(values?: {
            [Id: string]: number;
        }, EnumIsFlag?: boolean);
        /**
         * Returns the string (or LIST of strings separated by "," if EnumIsFlag == true) represented by the passed numeric value
         *
         * @method GetStringValue
         * @param {number} numericEnumValue
         * @returns {string}
         */
        GetStringValue(numericEnumValue: number): string;
        /**
         * Given a FLAG number and its ENUM definition returns an array of strings containing the
         * values represented by the flag number.
         * Useful for Kendo MVVM checkboxes
         *
         * @param flag The number (ex. 129)
         * @param enumType the EnumBaseType of the passed flag
         * @return {Array} ex. ["1", "128"]
         */
        static ConvertFlagToArray(flag: number, enumType: EnumBaseType): string[];
        /**
         * Given an array of strings containing the values represented by the flag
         * returns the computed flag number value (OR)
         * Useful for Kendo MVVM checkboxes
         *
         * @param flags The array of string values (ex. ["1", "128"] )
         * @param enumType the EnumBaseType of the passed flag
         * @return {Array} The computed flag number (ex. 129)
         */
        static ConvertArrayToFlag(flags: string[], enumType: EnumBaseType): number;
    }
    module Guid {
        function New(): any;
        /**
         * Returns a NEW GUID without dashes
         *
         * @method NewShort
         * @return {string}
         */
        function NewShort(): string;
        function Empty(): string;
        /**
         * Returns the short EMPTY GUID (without dashes)
         *
         * @method EmptyShort
         * @return {string}
         */
        function EmptyShort(): string;
        /**
         * Checks if the value is a string that contain a valid NON-EMPTY Guid (NORMAL or SHORT)
         * NO case-sensitive!
         *
         * @param value {string} the string to check
         * @method IsReady
         * @return {boolean}
         */
        function IsReady(value: string): boolean;
        /**
         * Returns TRUE if obj SEEMS to be a GUID
         *
         * @method IsGuid
         * @param obj What to check
         * @return {boolean}
         */
        function IsGuid(obj: any): obj is string;
        /**
         * Returns TRUE if obj SEEMS to be a SHORT GUID (without dashes)
         *
         * @method IsGuid
         * @param obj What to check
         * @return {boolean}
         */
        function IsShortGuid(obj: any): obj is string;
        /**
         * Checks if 2 Guids are equal (NORMAL or SHORT, case insensitive)
         *
         * @method Equals
         * @return {boolean}
         */
        function Equals(value1: string, value2: string): boolean;
    }
    /**
     * Following the current culture format (GetCurrentLanguage()) returns a NUMBER object or NULL if the string could not be parsed.
     *
     * @method ParseNumber
     * @param {string} str The string to parse
     * @return {number}
     */
    function ParseNumber(str: string): number;
    /**
     * Returns TRUE if the passed string represents the "true" boolean value (case INSENSITIVE check), FALSE otherwise
     *
     * @method ParseBoolean
     * @param {string} str The string to parse
     * @return {boolean}
     */
    function ParseBoolean(str: string): boolean;
    /**
     * Returns TRUE if the passed string is EMPTY or is an URL ( starts with http(s):// or ftp(s):// )
     *
     * @method StringIsUrl
     * @param {string} str The string to check
     * @return {boolean}
     */
    function StringIsUrl(str: string): boolean;
    function UrlToObject(url?: string): any;
    /**
     * Returns the CURRENT LANGUAGE
     *
     * @method GetCurrentLanguage
     * @return {string}
     */
    function GetCurrentLanguage(): string;
    /**
     * Changes the CURRENT LANGUAGE
     *
     * @method SetCurrentLanguage
     * @param val The value to set
     */
    function SetCurrentLanguage(val: string): void;
    /**
     * Given a LocalizedStringModel returns the localized value for a specific language
     * (or the CURRENT one if not specified)
     *
     * @method GetLocalizedValue
     * @return {string}
     */
    function GetLocalizedValue(localizedStringModel: {
        Values: any;
    }, language?: string): string;
    /**
     * Given an array of objects returns an object with all grouped keys mapping to the array of related items.
     *
     * @param arr {array} The array to cycle
     * @param key {string} The property to use to group items
     * @param keysComparer {Function} If specified, a custom key comparer (ex: case inensitive match?)
     * @return object
     * @method GroupBy<T>
     */
    function GroupBy<T>(arr: T[], key: string, keysComparer?: (a: string, b: string) => boolean): {
        [id: string]: T[];
    };
    /**
     * Generates a dummy integer HASH code representing the passed string using a dummy custom hashing function
     *
     * @param toHash {string} The string to be hashed
     * @return number
     * @method HashCode
     */
    function HashCode(toHash: string): number;
    /**
     * Returns an empty ID for a new "person" in BLOX URN format
     *
     * @return {string}
     */
    /**
     * Adds "search" function to standard Array object.
     * Does the same than "indexOf", but this is not supported by IE < 9
     * Can search anything: strings, objects, numbers, nulls, ....
     *
     * @method SearchInArray<T>
     *
     * @param {Array} Arr               The array to search into
     * @param {T} toSearch              [OBJ, FUN] The object to search throughout the array
     *                                  or a callback function receiving the element and returning a boolean value stating if the search should terminate
     * @param {boolean} [caseSensitive] (defaults to true): if true AND ToSearch is a string then checks case
     * @param {number} [from]           (defaults to 0): the starting index of the search in the array
     *
     * @return {number} The POSITION index of the searched element
     */
    function SearchInArray<T>(Arr: T[], toSearch: T | ((T: any) => boolean), caseSensitive?: boolean, from?: number): number;
    /**
     * Same as SearchInArray, but returns the found ITEM (not its position in array) if found, or NULL otherwise
     *
     * @method SearchValInArray<T>
     */
    function SearchValInArray<T>(Arr: T[], toSearch: T | ((T: any) => boolean), caseSensitive?: boolean, from?: number): T;
    /**
     * Searches the passed array and returns the FIRST ITEM with the specified "Id" field, or "undefined" if not found.
     * Similar to {{#crossLink "__/SearchInArray<T>"}}{{/crossLink}}
     *
     * @method SearchById<T>
     *
     * @param {Array} Arr               The array to search into
     * @param {string} id               The Id value to search
     * @param {boolean} [caseSensitive] (defaults to true): if true AND ToSearch is a string then checks case
     * @param {number} [from]           (defaults to 0): the starting index of the search in the array
     *
     * @returns {T} The FIRST item with the specified "Id" field
     */
    function SearchById<T extends {
        Id: string;
    }>(Arr: T[], id: string, caseSensitive?: boolean, from?: number): T;
    /**
     * Searches the passed array. If the element is found REMOVES it.
     *
     * @param {Array} array The array to search and modify
     * @param {object|function} element The element to search. If is a function it must return bool (TRUE when found)
     * @return {boolean} TRUE if found and removed, FALSE otherwise
     */
    function RemoveFromArray<T>(array: T[], element: T): boolean;
    /**
     * Removes the element at position "idx" of the passed "array"
     */
    function RemoveFromArrayByIdx(array: any[], idx: number): void;
    /**
     * Removes all elements in the passed array satisfying the passed condition
     *
     * @method RemoveFromArrayByCondition
     * @param {Array} array The array to alter
     * @param {function} condition The function checking if the element satisfies the condition for it to be removed from the array
     * @returns {number} the removed items count
     */
    function RemoveFromArrayByCondition<T>(array: T[], condition: (element: T) => boolean): number;
    /**
     * Insert an element into an array at the specified position.
     *
     * @method InsertInArray<T>
     * @param {Array} array The array to alter
     * @param {object|function} element The element to insert
     * @param {number} position The 0-based position where to insert the passed element
     * @return {boolean} TRUE if insertion succeeded, FALSE otherwise
     */
    function InsertInArray<T>(array: T[], element: T, position: number): boolean;
    function RandomString(stringLength: number, chars?: string): string;
    /**
     * DEEP CLONING of a "thing" which can be a NATIVE type or an object.
     * The ONLY thing which IS NOT cloned (but copied by reference) are DOM NODES
     *
     * KNOWN ISSUE: objects are cloned calling their constructor WITHOUT ANY PARAMETER!!!!
     *
     * @method CloneObj<T>
     * @param {T} obj The "thing" to clone
     * @return {T} A perfect COPY of the passed "thing"
     */
    function CloneObj<T>(obj: T): T;
    /**
     *  Calls a passed callback for each NESTED property of the passed object
     *
     * @method CycleObjectNestedProperties
     * @param obj The object to traverse
     * @param onProp Callback to be called on each property found. Receives the property path, value and parent object. If returns FALSE the traversing is stopped
     * @param callbackOnNestedObjectsToo If TRUE "onProp" get fired also for properties holding nested objects
     */
    function CycleObjectNestedProperties(obj: object, onProp: (path: string, value: any, parentObject: object) => boolean, callbackOnNestedObjectsToo?: boolean): void;
    /**
     * Returns a NEW object with all properties of EXTENSION and the ones in BASE which are not present in EXTENSION.
     * NULL values in extension are not ignored but COPIED (overwrite value in base object), UNDEFINED ones are IGNORED!
     * Nested properties are copied with the same logic
     * IF MORE than 2 arguments are passed all RIGHT ones are merged sequentially in the LEFT ones
     *
     * Ex.          MergeObj(A, B, C, D, E)
     * Expands to:  let AB = MergeObj(A, B)
     *              let ABC = MergeObj(AB, C)
     *              let ABCD = MergeObj(ABC, D)
     *              return MergeObj(ABCD, E)
     *
     * @method MergeAnyObj
     * @param params The objects to be merged
     * @returns {object} The merged result
     */
    function MergeAnyObj(...params: any[]): any;
    /**
     * Returns a NEW object with all properties of EXTENSION and the ones in BASE which are not present in EXTENSION.
     * NULL values in extension are not ignored but COPIED (overwrite value in base object), UNDEFINED ones are IGNORED!
     * Nested properties are copied with the same logic
     *
     * Ex.          MergeObj(A, B, C, D, E)
     * Expands to:  let AB = MergeObj(A, B)
     *              let ABC = MergeObj(AB, C)
     *              let ABCD = MergeObj(ABC, D)
     *              return MergeObj(ABCD, E)
     *
     * @returns {object} The merged result
     *
     * @see MergeAnyObj
     * @method MergeObj
     * @param base The BASE object to be extended
     * @param extension The extension to be added to BASE
     */
    function MergeObj<T1, T2>(base: T1, extension: T2): T1 & T2;
    /**
     * Returns all the KEYS of the passed object as an ARRAY of strings, or NULL if the passed obj IS NOT an object
     *
     * @method ObjKeysToArray
     * @param obj
     * @return {null|string[]}
     */
    function ObjKeysToArray(obj: object): string[];
    /**
     * Returns an property (eventually nested) traversing the passed object.
     * Returns the passed object if NO path is given, UNDEFINED if not found.
     *
     * @method GetObjProperty
     * @param obj The object to be traversed. EX: {prop1 : {prop2 : 33}}
     * @param path The path to the requested property. EX: "prop1.prop2"
     */
    function GetObjProperty(obj: object, path: string): any;
    /**
     * Wraps a function with a passed one. Features:
     *
     * - it's possible to run the passed "newFun" BEFORE or AFTER the original one
     * - the original context is preserved
     * - "newFun" will receive an additional parameter with infos and options:
     *   - "returnMyReturn" option: control which return object to be used
     *   - "proceed" option: control whether or not to call the original function
     *   - "argumentsToPass" option: customize the passed arguments to the original function
     *   - "originalReturn" option: the original returned object (if "before" == FALSE)
     *
     * @param newFun [function]
     *   The new function (will receive an additional parameter with infos and options)
     *
     * @param funName [string]
     *   The function name to wrap
     *
     * @param [funContext] [object]
     *   The context object holding the member "funName".
     *   It will be the "this" for both functions.
     *   Defaults to the global "window" object
     *
     * @param [before] [boolean]
     *   If TRUE "newFun" will be called before the original,
     *   otherwise after. Defaults to TRUE
     *
     * @param [callOnDynamicThis] [boolean]
     *   If TRUE "funContext" will be ignored as "this" and both functions
     *   will be run on the "run-time" object "this". Defaults to TRUE
     */
    function WrapFun(newFun: Function, funName: string, funContext?: any, before?: boolean, callOnDynamicThis?: boolean): void;
    /**
     * Reverts to the original a wrapped function created with __.WrapFun(...)
     *
     * @param funName [string] The function name to restore
     * @param [funContext] [object] The context object holding the member "funName". Defaults to the global "window" object
     */
    function UnwrapFun(funName: any, funContext: any): void;
    /**
     * Converts an object to another following the passed rules.
     *
     * @method MapObject
     *
     * @param obj The object to convert
     *
     * @param {string[][]} mappings  The conversion rules.
     * It must be an array of 2 strings arrays.
     * The first states the SOURCE property to be copied, the second the DESTINATION property name.
     * Those properties can be NESTED. Ex: "subObject.subProp"
     *
     * @param {boolean} [ignoreMissingProperties = true] If TRUE when a source property is not found
     * then NULL will be returned (situation threaten as an error)
     *
     * @param {boolean} [cloneObjects = false] If TRUE the converted properties will be a CLONE of the source ones.
     * Otherwise a reference to the same object will be created.
     *
     * @return {Object|null} The converted object or NULL upon errors
     */
    function MapObject(obj: any, mappings: any, ignoreMissingProperties: any, cloneObjects: any): {};
    /**
     * Return the STRING representing the passed ENUM value (for a TypeScript ENUM object)
     *
     * @param e The TypeScript ENUM object
     * @param val The integer value of the ENUM
     */
    function GetEnumKey(e: any, val: number): string;
    class FluentParallel {
        private functions;
        constructor(firstFunction?: (onEnd: (result?: any) => void) => any);
        Parallel(otherFunction: (onEnd: (result?: any) => void) => any): FluentParallel;
        ExecuteAndJoin(joinFunction: (resultsOrderedByPosition: ExecuteAndJoin.Result[], resultsOrderedByTime: ExecuteAndJoin.Result[], timeoutException?: Error) => void): void;
    }
    /**
     * Helper function offering FLUENT syntax for __.ExecuteAndJoin
     *
     * Calls can be joined:
     * __.Parallel(function(onEnd){})
     *   .Parallel(function(onEnd){})
     *   .Parallel(function(onEnd){})
     *   .ExecuteAndJoin(function(resultsOrderedByPosition, resultsOrderedByTime, timeoutException){})
     *
     * @param {(onEnd: (result?: any) => void) => any} firstFunction Eventual first function to add to the list
     */
    function Parallel(firstFunction?: (onEnd: (result?: any) => void) => any): FluentParallel;
    module ExecuteAndJoin {
        interface Result {
            Position: number;
            SuccessfulResult: any;
            Exception: Error;
        }
    }
    /**
     * Executes ALL passed functions meant to be ASYNCHRONOUS passing to them as only argument a "END CALLBACK" to be called
     * with the result to ce collected.
     *
     * The LAST argument is the "JOIN" function: it will be called when EVERY passed function has called its own "END CALLBACK"
     * or has thrown an exception.
     * This "JOIN" function accepts 2 arrays with all results and an eventual Timeout error (if ALL functions did not call their own "END CALLBACK" in time (ExecuteAndJoin.TimeoutMs)):
     *   the FIRST ordered by the passed functions positions,
     *   the SECOND ordered by "END CALLBACK" call time.
     *
     * <b>WARNING</b>: the return values of the passed functions are IGNORED (the captured one is what is passed to the "END CALLBACK")<br>
     * <b>WARNING</b>: in the passed functions don't execute other code after "END CALLBACK" is called, just return.<br>
     * <b>WARNING</b>: if a passed function never calls its "END CALLBACK", then the "JOIN" function will never be called<br>
     * <b>WARNING</b>: multiple calls to an "END CALLBACK" are ignored (only the first one is considered)<br>
     *
     * @example
     *    ExecuteAndJoin(asyncFun1, asyncFun2, joinFun);
     *
     * @method ExecuteAndJoin
     */
    function ExecuteAndJoin(...params: any[]): void;
    module ExecuteAndJoin {
        let TimeoutMs: number;
    }
    /**
     *   Returns a function, that, as long as it continues to be invoked, will not
     *   be triggered. So the function will be called ONLY ONCE, after it stops being called for
     *   N milliseconds.
     *   If `immediate` is passed, triggers the function on its FIRST invocation, instead on the timeout expiration.
     *
     * @param func The function to be called only ONCE
     * @param waitForMs The timeout milliseconds
     * @param immediate If TRUE the  `func` is called upon the first invocation
     */
    function Debounce(func: () => void, waitForMs: number, immediate?: boolean): () => void;
    module Dates {
        /**
         * Checks that the passed argument is a string in the Microsoft format like "\/Date(1405728000000)\/"
         *
         * @method IsMsFormatStringDate
         * @param msFormatString
         * @return {boolean}
         */
        function IsMsFormatStringDate(msFormatString: any): boolean;
        /**
         * Checks that the passed argument is a string in the ISO format like "2014-12-30T15:20:00Z"
         *
         * @method IsMsFormatStringDate
         * @param isoFormatString
         * @return {boolean}
         */
        function IsISOFormatStringDate(isoFormatString: any): boolean;
        /**
         * Given an ISO string like "2014-12-30T15:20:00Z" returns the corresponding date
         * CONVERTED to the local time.
         * Has a fallback to manual parsing for older IEs (TODO: without considering the time shift for now...)
         *
         * @method GetDateFromISO
         * @param {string|*} isoString a string like "2014-12-30T15:20:00Z"
         * @return {Date|null} Returns the DATE object or NULL if the string is not valid
         */
        function GetDateFromISO(isoString: Date | string): Date;
        /**
         * Given a Microsoft formatted string like "\/Date(1239018869048)\/"  returns the corresponding date
         *
         * @method GetDateFromMsFormat
         * @param {Date | string} msFormatString
         * @returns {Date}
         */
        function GetDateFromMsFormat(msFormatString: Date | string): Date;
        /**
         * Returns a Date object shifted of the current UTC timespan
         * (useful when need to print in the Local form a json UTC DATE received from the server)
         *
         * @method AddUtcOffsetToDate
         * @param date The date to shift
         * @param [editPassedDate] {boolean} If TRUE the passed date object will be modified, otherwise not. DEFAULTS to FALSE
         * @returns {Date} The shifted date object
         */
        function AddUtcOffsetToDate(date: Date | string, editPassedDate: boolean): Date;
        /**
         * Returns a Date object shifted BACK of the current UTC timespan
         * (useful when need to send the json date in UTC starting form a Locale DATE)
         *
         * @method SubtractUtcOffsetToDate
         * @param date The date to shift
         * @param [editPassedDate] {boolean} If TRUE the passed date object will be modified, otherwise not. DEFAULTS to FALSE
         * @returns {Date} The shifted date object
         */
        function SubtractUtcOffsetToDate(date: any, editPassedDate: any): Date;
        /**
         * Useful to handle DATE-ONLY objects discarding the time (ex. birthdate!!!) without problems with server.
         *
         * @method DatePartOnly
         * @param {Date} date
         * @returns {Date} A new date without the TIME (00:00)
         */
        function DatePartOnly(date: any): Date;
        /**
         * Returns the difference of LocalTime with UTC in MINUTES
         * (applying the current daylight time saving)
         *
         * @method GetUtcOffset
         * @returns {number}
         */
        function GetUtcOffset(): number;
        function FormatDate(date: Date, showYear?: boolean, showMonth?: boolean, showDay?: boolean, showHour?: boolean, showMinute?: boolean, showSecond?: boolean, showMillisecond?: boolean): string;
    }
}
