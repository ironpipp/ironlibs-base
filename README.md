# IronLibs

Ironpipp's useful utilities for both nodejs, browser with jQuery or Angular. 

This repo holds base common utilities for any environment.

## Features
- Fully typed
- by default no dependencies are needed. Are required ONLY when used 
    - **node-uuid**        for IronLibsCommon.Guid.New(). In browser simply include its .JS file BEFORE IronLibs
    - **sprintf-js**       for IronLibsCommon.Format(). In browser simply include its .JS file BEFORE IronLibs
    - **uWebSockets.js**   NodeJs only, for IronLibsBus.Bus.ListenForRemoteConnection()
   
### How to build and test
     > npm run build
